﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MyTotalCommander.Commands
{
    public class ButtonCommands
    {
        private static RoutedUICommand _search;
        private static RoutedUICommand _select;
        private static RoutedUICommand _close;
        private static RoutedUICommand _edit;
        private static RoutedUICommand _copy;
        private static RoutedUICommand _move;
        private static RoutedUICommand _newFolder;
        private static RoutedUICommand _delete;

        static ButtonCommands()
        {
            Dictionary<string, InputGestureCollection> inputs = new Dictionary<string, InputGestureCollection>();
            inputs.Add("Search", new InputGestureCollection());
            inputs.Add("Select", new InputGestureCollection());
            inputs.Add("Close", new InputGestureCollection());
            inputs.Add("Edit", new InputGestureCollection());
            inputs.Add("Copy", new InputGestureCollection());
            inputs.Add("Move", new InputGestureCollection());
            inputs.Add("New", new InputGestureCollection());
            inputs.Add("Delete", new InputGestureCollection());
            inputs["Search"].Add(new KeyGesture(Key.F7, ModifierKeys.Alt, "Alt+F7"));
            inputs["Close"].Add(new KeyGesture(Key.F4, ModifierKeys.Alt, "Alt+F4"));
            inputs["Select"].Add(new MouseGesture(MouseAction.LeftClick));
            inputs["Edit"].Add(new KeyGesture(Key.F4, ModifierKeys.None, "F4"));
            inputs["Copy"].Add(new KeyGesture(Key.F5, ModifierKeys.None, "F5"));
            inputs["Move"].Add(new KeyGesture(Key.F6, ModifierKeys.None, "F6"));
            inputs["New"].Add(new KeyGesture(Key.F7, ModifierKeys.None, "F7"));
            inputs["Delete"].Add(new KeyGesture(Key.F8, ModifierKeys.None, "F8"));
            inputs["Delete"].Add(new KeyGesture(Key.Delete, ModifierKeys.None, "Delete"));
            _search = new RoutedUICommand("Search", "Search", typeof(ButtonCommands), inputs["Search"]);
            _select = new RoutedUICommand("Select", "Select", typeof(ButtonCommands), inputs["Select"]);
            _close = new RoutedUICommand("Close", "Close", typeof(ButtonCommands), inputs["Close"]);
            _edit = new RoutedUICommand("Edit", "Edit", typeof(ButtonCommands), inputs["Edit"]);
            _copy = new RoutedUICommand("Copy", "Copy", typeof(ButtonCommands), inputs["Copy"]);
            _move = new RoutedUICommand("Move", "Move", typeof(ButtonCommands), inputs["Move"]);
            _newFolder = new RoutedUICommand("NewFolder", "NewFolder", typeof(ButtonCommands), inputs["New"]);
            _delete = new RoutedUICommand("Delete", "Delete", typeof(ButtonCommands), inputs["Delete"]);
        }

        public static RoutedUICommand Select
        {
            get { return _select; }
        }

        public static RoutedUICommand Close
        {
            get { return _close; }
        }

        public static RoutedUICommand Edit
        {
            get { return _edit; }
        }

        public static RoutedUICommand Copy
        {
            get { return _copy; }
        }

        public static RoutedUICommand Move
        {
            get { return _move; }
        }

        public static RoutedUICommand NewFolder
        {
            get { return _newFolder; }
        }

        public static RoutedUICommand Delete
        {
            get { return _delete; }
        }

        public static RoutedUICommand Search
        {
            get { return _search; }
        }
    }
}
