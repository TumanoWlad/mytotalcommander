﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using FileSystemClasses;
using System.IO;
using System.Globalization;

namespace MyTotalCommander.Conversions
{
    [ValueConversion(typeof(DriveType), typeof(string))]
    public class ImagePathConvertor : IValueConverter
    {
        public static readonly string CDRomImage = "\\Images\\OpticalDisk.png";
        public static readonly string FixedDriveImage = "\\Images\\HardDrive.png";
        public static readonly string RemovableDrive = "\\Images\\ExternalDrive.ico";

        public object Convert(object value, Type targetType, object parametr, CultureInfo culture)
        {
            DriveType type = (DriveType)value;
            if (type == DriveType.CDRom)
                return CDRomImage;
            else if (type == DriveType.Removable)
                return RemovableDrive;
            else
                return FixedDriveImage;
        }

        public object ConvertBack(object value, Type targetType, object parametr, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
