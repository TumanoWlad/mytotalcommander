﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FileSystemClasses.FileSystemItems
{
    public abstract class TextSearchStrategy
    {
        public abstract bool SearchText(string line);
    }

    public class SimpleSearchStrategy : TextSearchStrategy
    {
        private bool _caseSensitive;
        private string _text;

        public SimpleSearchStrategy(string text, bool caseSensitive)
        {
            _caseSensitive = caseSensitive;
            if (_caseSensitive)
                _text = text;
            else
                _text = text.ToUpperInvariant();
        }

       public override bool SearchText(string line)
        {
            if (!_caseSensitive && line.ToUpperInvariant().Contains(_text) || _caseSensitive && line.Contains(_text))
                return true;

            return false;
        }
    }

    public class RegExSearchStrategy : SimpleSearchStrategy
    {
        private Regex _regularExpression;

        public RegExSearchStrategy(string text, bool caseSensitive, Regex regEx): base(text, caseSensitive)
        {
            _regularExpression = regEx;
        }

        public override bool SearchText(string line)
        {
            if (_regularExpression.IsMatch(line))
                return true;

            return base.SearchText(line);
        }
    }

    public class WholeWordsSerachStrategy : TextSearchStrategy
    {
        private Regex _regularExpression;

        public WholeWordsSerachStrategy(string text, bool caseSensitive)
        {
            _regularExpression = new Regex(String.Format("\\b{0}\\b", text), RegexOptions.Compiled | (caseSensitive ? RegexOptions.None : RegexOptions.IgnoreCase));
        }

        public override bool SearchText(string line)
        {
            if (_regularExpression.IsMatch(line))
                return true;

            return false;
        }
    }
}
