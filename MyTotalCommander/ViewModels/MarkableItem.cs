﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace MyTotalCommander
{
    public class MarkableItem<T> : INotifyPropertyChanged
    {
        private bool _isMarked;
        private bool _isSelected;

        public T Item
        {
            get;
            private set;
        }

        public bool IsMarked
        {
            get { return _isMarked; }
            set { _isMarked = value; OnPropertyChanged("IsMarked"); }
        }

        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; OnPropertyChanged("IsSelected"); }
        }

        public MarkableItem(T item)
        {
            Item = item;
            _isMarked = false;
        }

        private void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
