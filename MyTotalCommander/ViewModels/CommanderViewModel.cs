﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using FileSystemClasses;
using System.IO;
using System.Windows;
using System.Diagnostics;

namespace MyTotalCommander
{
    public class CommanderViewModel : INotifyPropertyChanged
    {

        public List<Drive> drives = new List<Drive>();
        public List<MarkableItem<FSItemContent>> Items;

        private Drive _currentDrive;
        private string _path;

        public string Path
        {
            get
            {
                return _path;
            }
            private set
            {
                _path = value + "\\*.*";
                OnPropertyChanged("Path");
            }
        }

        public CommanderViewModel(int driveIndex)
        {
            drives = Drive.ReadSystemDrives();
            if (driveIndex > drives.Count)
                driveIndex = 0;
            SetDrive(drives[driveIndex]);
        }

        public void SetDrive(Drive drive)
        {
            Items = PackItems(drive.GetContent());
            Path = drive.Letter.ToString().ToUpper() + ":";
            _currentDrive = drive;
        }

        private List<MarkableItem<FSItemContent>> PackItems(List<FSItemContent> itemList)
        {
            List<MarkableItem<FSItemContent>> packedItems = new List<MarkableItem<FSItemContent>>();
            foreach (var item in itemList)
            {
                MarkableItem<FSItemContent> newPackedItem = new MarkableItem<FSItemContent>(item);

                if (item.Name == "_ _")
                    newPackedItem.IsSelected = true;

                packedItems.Add(newPackedItem);

            }
            return packedItems;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public void Open(FSItemContent clickedItem)
        {
            try
            {
                if (clickedItem.HasAttribute(FileAttributes.Directory))
                {
                    // open folder
                    List<FSItemContent> newContent = new List<FSItemContent>();

                    if (clickedItem.Name == "_ _")
                    {
                        string newPath = clickedItem.Path.Remove(clickedItem.Path.LastIndexOf("\\")) + "\\";
                        newContent = Drive.GetContent(newPath);
                        if (newPath.Split('\\').Length != 2)
                        {
                            DirectoryInfo topDirInfo = new DirectoryInfo(newPath.Remove(newPath.LastIndexOf("\\")));
                            FolderContent topFolder = new FolderContent(topDirInfo);
                            topFolder.SetAsReturnItem();
                            newContent.Insert(0, topFolder);
                        }
                        Path = newPath.Remove(newPath.LastIndexOf('\\'));
                    }
                    else
                    {
                        newContent = clickedItem.GetContent();
                        clickedItem.SetAsReturnItem();
                        newContent.Insert(0, clickedItem);
                        Path = clickedItem.Path;
                    }
                    Items = PackItems(newContent);
                }
                else // open file in default program
                    clickedItem.Open();
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Administrator rights required.");
            }
        }

        public void RefreshItems()
        {
            if (Items[0].Item.Name == "_ _")
            {
                List<FSItemContent> newContent = Drive.GetContent(Path.Remove(Path.LastIndexOf('\\') + 1));
                newContent.Insert(0, Items[0].Item);
                Items = PackItems(newContent);
            }
            else
                Items = PackItems(Drive.GetContent(Path.Remove(Path.LastIndexOf('\\') + 1)));
        }

        public static void Edit(string Path)
        {
            Process.Start("Notepad.exe", Path);
        }
    }
}
