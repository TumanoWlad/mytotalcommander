﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows;
using System.ComponentModel;
using System.Threading;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic.FileIO;
using System.Windows.Data;
using System.Collections.ObjectModel;
using System.Collections.Concurrent;
using FileSystemClasses.FileSystemItems;

namespace MyTotalCommander
{
    public class SearchViewModel : INotifyPropertyChanged
    {
        private static volatile SearchViewModel _instance;
        private volatile bool _searchTaskInProgress;
        private static object syncRoot = new Object();
        private static readonly int _threadCount = 4;
        private enum State { Searching, SearchCompleted, Canceled };
        private static readonly Dictionary<State, string> StateStrings = new Dictionary<State, string>();
        private HashSet<Encoding> encodingsToSearch;
        private State _state;
        private ConcurrentQueue<string> _currentSearchResults;
        private enum Signs { Equal, Greater, Less };
        private enum SizeMeasures { Byte, Kbyte, Mbyte, Gbyte };
        private enum TimeMeasures { Minutes, Hours, Days, Weeks, Months, Years };
        private CancellationTokenSource _tokenSource;
        private CancellationToken _token;
        private TextSearchStrategy _searchStrategy;
        private static readonly int _clusterSize = 4096;


        public string CurrentState
        {
            get
            {
                return StateStrings[_state];
            }
            set
            {
                if (value == StateStrings[State.Searching])
                    _state = State.Searching;
                else if (value == StateStrings[State.SearchCompleted])
                    _state = State.SearchCompleted;
                else
                    _state = State.Canceled;
                OnPropertyChanged("CurrentState");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        
        public ObservableCollection<string> SearchResults
        {
            get;
            private set;
        }

        /*File search properties*/

        private int _level;
        public int Level
        {
            get
            {
                return _level;
            }
            set
            {
                _level = value;
            }
        }

        public string SearchPath
        {
            get;
            set;
        }

        private Regex _searchRegEx;
        public string SearchPattern
        {
            get;
            set;
        }

        public bool RegExPath
        {
            get;
            set;
        }

        /*Text search properties*/
        private Regex _textRegEx;
        public string Text
        {
            get;
            set;
        }

        public bool RegExText
        {
            get;
            set;
        }

        public bool SearchText
        {
            get;
            set;
        }

        public bool WholeWords
        {
            get;
            set;
        }    

        public bool CaseSensitive
        {
            get;
            set;
        }

        private bool _noText;
        public bool NoTextIn
        {
            set
            {
                _noText = value;
            }
        }

        public bool ANSI
        {
            set
            {
                if (value)
                    encodingsToSearch.Add(Encoding.Default);
                else
                    encodingsToSearch.Remove(Encoding.Default);
            }
        }

        public bool ASCII
        {
            set
            {
                if (value)
                    encodingsToSearch.Add(Encoding.ASCII);
                else
                    encodingsToSearch.Remove(Encoding.ASCII);
            }
        }

        public bool UTF8
        {
            set
            {
                if (value)
                    encodingsToSearch.Add(Encoding.UTF8);
                else
                    encodingsToSearch.Remove(Encoding.UTF8);
            }
        }

        public bool UTF16
        {
            set
            {
                if (value)
                    encodingsToSearch.Add(Encoding.Unicode);
                else
                    encodingsToSearch.Remove(Encoding.Unicode);
            }
        }

        /*Advanced search properties*/

        public bool DateBetween
        {
            get;
            set;
        }

        private DateTime _dateFrom;

        public DateTime DateFrom
        {
            get
            {
                return _dateFrom;
            }
            set
            {
                _dateFrom = value;
                OnPropertyChanged("DateFrom");
            }
        }

        private DateTime _dateTo;

        public DateTime DateTo
        {
            get
            {
                return _dateTo;
            }
            set
            {
                _dateTo = value;
                OnPropertyChanged("DateTo");
            }
        }

        public bool NotOlder
        {
            get;
            set;
        }

        private TimeSpan _timeSpan;

        public string Period
        {
            set
            {
                int span = 0;
                if (int.TryParse(value, out span))
                    _timeSpan = new TimeSpan(_timeMeasure == TimeMeasures.Years ? 365 * span : 0 + _timeMeasure == TimeMeasures.Months ? 30 * span : 0 + _timeMeasure == TimeMeasures.Weeks ? 7 * span : 0 + _timeMeasure == TimeMeasures.Days ? span : 0, 
                        _timeMeasure == TimeMeasures.Hours ? span : 0, _timeMeasure == TimeMeasures.Minutes ? span : 0, 0);
            }
        }

        private TimeMeasures _timeMeasure;

        public string TimeMeasure
        {
            set
            {
                switch(value)
                {
                    case "Minute(s)":
                        _timeMeasure = TimeMeasures.Minutes;
                        break;
                    case "Hour(s)":
                        _timeMeasure = TimeMeasures.Hours;
                        break;
                    case "Day(s)":
                        _timeMeasure = TimeMeasures.Days;
                        break;
                    case "Week(s)":
                        _timeMeasure = TimeMeasures.Weeks;
                        break;
                    case "Month(s)":
                        _timeMeasure = TimeMeasures.Months;
                        break;
                    case "Year(s)":
                        _timeMeasure = TimeMeasures.Years;
                        break;
                    default:
                        break;
                }
            }
        }

        public bool FileSize
        {
            get;
            set;
        }

        private Signs _sign;

        public string Sign
        {
            set
            {
                switch (value)
                {
                    case "=":
                        _sign = Signs.Equal;
                        break;
                    case ">":
                        _sign = Signs.Greater;
                        break;
                    case "<":
                        _sign = Signs.Less;
                        break;
                    default:
                        break;
                }
            }
        }

        private long _size;
        public string Size
        {
            get
            {
                return _size.ToString();
            }
            set
            {
                long.TryParse(value, out _size);
            }
        }

        private SizeMeasures _sizeMeasure;

        public string SizeMeasure
        {
            set
            {
                switch (value)
                {
                    case "Gbytes":
                        _sizeMeasure = SizeMeasures.Gbyte;
                        break;
                    case "Kbytes":
                    _sizeMeasure = SizeMeasures.Kbyte;
                        break;
                    case "Mbytes":
                        _sizeMeasure = SizeMeasures.Mbyte;
                        break;
                    case "bytes":
                        _sizeMeasure = SizeMeasures.Byte;
                        break;
                    default: 
                        break;
                }
            }
        }

        public bool Attributes
        {
            get;
            set;
        }

        public bool? ArchiveAttr
        {
            get;
            set;
        }

        public bool? ReadOnlyAttr
        {
            get;
            set;
        }

        public bool? HiddenAttr
        {
            get;
            set;
        }

        public bool? SystemAttr
        {
            get;
            set;
        }

        public bool? DirectoryAttr
        {
            get;
            set;
        }

        public bool? CompressedAttr
        {
            get;
            set;
        }

        public bool? EncryptedAttr
        {
            get;
            set;
        }

        static SearchViewModel()
        {
            StateStrings.Add(State.SearchCompleted, "Search Completed");
            StateStrings.Add(State.Searching, "Searching");
            StateStrings.Add(State.Canceled, "Canceled");
        }

        private SearchViewModel()
        {
            SearchResults = new ObservableCollection<string>();
            Level = 1;
            _tokenSource = new CancellationTokenSource();
            _token = _tokenSource.Token;
            BindingOperations.EnableCollectionSynchronization(SearchResults, syncRoot);
            encodingsToSearch = new HashSet<Encoding>();
            DateFrom = DateTo = DateTime.Now;
        }

        private void DefineSearchStrategy()
        {
            if (RegExText)
                _searchStrategy = new RegExSearchStrategy(Text, CaseSensitive, _textRegEx);
            else if (WholeWords)
                _searchStrategy = new WholeWordsSerachStrategy(Text, CaseSensitive);
            else
                _searchStrategy = new SimpleSearchStrategy(Text, CaseSensitive);
        }

        public void CancelSearch()
        {
            _tokenSource.Cancel();
        }

        private void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
        
        public static SearchViewModel Instance
        {
            get
            {
                if (_instance == null)
                    lock (syncRoot)
                    {
                        if (_instance == null)
                            _instance = new SearchViewModel();
                    }
                return _instance;
            }
        }

        public void Search()
        {
            _tokenSource = new CancellationTokenSource();
            _token = _tokenSource.Token;
            CurrentState = StateStrings[State.Searching];
            SearchResults.Clear();
            if (SearchText)
            {
                if (RegExPath)
                {
                    try
                    {
                        _searchRegEx = new Regex(SearchPattern, RegexOptions.IgnoreCase | RegexOptions.Compiled);
                    }
                    catch(ArgumentException)
                    {
                        _searchRegEx = null;
                    }
                }

                if (RegExText)
                {
                    try
                    {
                        _textRegEx = new Regex(Text, RegexOptions.Compiled | (CaseSensitive ? RegexOptions.None : RegexOptions.IgnoreCase));
                    }
                    catch(ArgumentException)
                    {
                        RegExText = false;
                        _textRegEx = null;
                    }
                }
                AdvancedTextSearch();

            }
            else
            {
                 FindFilesRecursive(SearchPath, _level);
            }
            if (_token.IsCancellationRequested)
                CurrentState = StateStrings[State.Canceled];
            else
                CurrentState = StateStrings[State.SearchCompleted];
        }

        private void FindFilesRecursive(string directory, int level)
        {
            if (!_token.IsCancellationRequested)
            {
                if (!DateBetween && !NotOlder && !FileSize && !Attributes)
                {
                    if (RegExPath && _searchRegEx != null)
                        Directory.GetFiles(directory).Where(file => _searchRegEx.IsMatch(file)).ToList().ForEach(SearchResults.Add);
                    else
                        Directory.GetFiles(directory, SearchPattern, System.IO.SearchOption.TopDirectoryOnly).ToList().ForEach(SearchResults.Add);
                }
                else
                {
                    if (RegExPath && _searchRegEx != null)
                        FilterFiles(Directory.GetFiles(directory).Where(file => _searchRegEx.IsMatch(file)).ToList()).ForEach(SearchResults.Add);
                    else
                        FilterFiles(Directory.GetFiles(directory, SearchPattern, System.IO.SearchOption.TopDirectoryOnly).ToList()).ForEach(SearchResults.Add);
                }
                if (level > 0 && !_token.IsCancellationRequested)
                {
                    List<string> subdirs = new List<string>();
                    subdirs = Directory.GetDirectories(directory).ToList();
                    foreach (string dir in subdirs)
                    {
                        try
                        {
                            FindFilesRecursive(dir, level - 1);
                        }
                        catch (UnauthorizedAccessException) { /* just skip such files */ }
                    }
                }
            }
        }

        private void FindTextRecursive(string directory, int level)
        {
            if (!_token.IsCancellationRequested)
            {
                FileSystem.FindInFiles(directory, Text, !CaseSensitive, Microsoft.VisualBasic.FileIO.SearchOption.SearchTopLevelOnly).ToList().ForEach(SearchResults.Add);
                if (level > 1 && !_token.IsCancellationRequested)
                {
                    List<string> subdirs = new List<string>();
                    subdirs = Directory.GetDirectories(directory).ToList();
                    foreach (string dir in subdirs)
                    {
                        try
                        {
                            FindTextRecursive(dir, level - 1);
                        }
                        catch (UnauthorizedAccessException) { /* just skip such files */ }
                    }
                }
            }
        }

        private void AdvancedTextSearch()
        {
            _searchTaskInProgress = true;
            _currentSearchResults = new ConcurrentQueue<string>();
            DateTime start = DateTime.Now;
            DefineSearchStrategy();
            Task.Run(() => CollectFiles());
            for (int i = 0; i < _threadCount; i++)
                Task.Run(() => CheckFiles()).Wait();
            TimeSpan passed = DateTime.Now - start;
            SearchResults.Insert(0, String.Format("[{0} files found]", SearchResults.Count));
        }

        private void CollectFiles()
        {
            CollectFilesRecursive(SearchPath, _level);
            _searchTaskInProgress = false;
        }

        private void CheckFiles()
        {
            Thread.CurrentThread.Priority = ThreadPriority.Highest;
            string fileName;
            int shift = Text.Length - 1;
            int bufSize = _clusterSize + shift;
            char[] buffer;
            bool found;
            while ((_searchTaskInProgress || !_currentSearchResults.IsEmpty) && !_token.IsCancellationRequested)
            {
                if (!_currentSearchResults.TryDequeue(out fileName))
                {
                    Thread.Sleep(5);
                    continue;
                }
                found = false;
                foreach (Encoding encoding in encodingsToSearch)
                {
                    try
                    {
                        using (StreamReader reader = new StreamReader(fileName, encoding))
                        {
                            buffer = new char[bufSize];
                            do
                            {
                                reader.ReadBlock(buffer, shift, bufSize - shift);
                                if (_searchStrategy.SearchText(new string(buffer)))
                                {
                                    if (!_noText)
                                        SearchResults.Add(fileName);
                                    found = true;
                                    break;
                                }
                                else
                                {
                                    for (int i = 0, j = bufSize - shift; i < shift; i++, j++)
                                        buffer[i] = buffer[j];
                                }
                            }
                            while (!reader.EndOfStream && !_token.IsCancellationRequested);
                        }
                    }
                    catch (IOException)
                    {
                        break;
                    }
                    if (found)
                        break;
                }
                if (_noText && !found)
                    SearchResults.Add(fileName);
            }
            Thread.CurrentThread.Priority = ThreadPriority.Normal;
        }

        private void CollectFilesRecursive(string directory, int level)
        {
            if (!_token.IsCancellationRequested)
            {
                if (!DateBetween && !NotOlder && !FileSize && !Attributes)
                {
                    if (RegExPath && _searchRegEx != null)
                        Directory.GetFiles(directory).Where(file => _searchRegEx.IsMatch(file)).ToList().ForEach(_currentSearchResults.Enqueue);
                    else
                        Directory.GetFiles(directory, SearchPattern, System.IO.SearchOption.TopDirectoryOnly).ToList().ForEach(_currentSearchResults.Enqueue);
                }
                else
                {
                    if (RegExPath && _searchRegEx != null)
                        FilterFiles(Directory.GetFiles(directory).Where(file => _searchRegEx.IsMatch(file)).ToList()).ForEach(_currentSearchResults.Enqueue);
                    else
                        FilterFiles(Directory.GetFiles(directory, SearchPattern, System.IO.SearchOption.TopDirectoryOnly).ToList()).ForEach(_currentSearchResults.Enqueue);
                }

                if (level > 1 && !_token.IsCancellationRequested)
                {
                    List<string> subdirs = new List<string>();
                    subdirs = Directory.GetDirectories(directory).ToList();
                    foreach (string dir in subdirs)
                    {
                        try
                        {
                            CollectFilesRecursive(dir, level - 1);
                        }
                        catch (UnauthorizedAccessException) { /* just skip such files */ }
                    }
                }
            }
        }

        private List<string> FilterFiles(List<string> files)
        {
            List<string> filteredFiles = new List<string>();
            if (NotOlder)
                DateFrom = DateTime.Now - _timeSpan;
            DateTime writeTime;
            foreach (string file in files)
            {
                if (DateBetween)
                {
                    writeTime = File.GetLastWriteTime(file);
                    if (writeTime > _dateTo || writeTime < _dateFrom)
                        continue;
                }
                else if (NotOlder)
                {
                    writeTime = File.GetLastWriteTime(file);
                    if (writeTime < DateFrom)
                        continue;
                }

                if (FileSize)
                {
                    FileInfo info = new FileInfo(file);
                    long size = 0;
                    if (_sizeMeasure == SizeMeasures.Byte)
                        size = _size;
                    else if (_sizeMeasure == SizeMeasures.Kbyte)
                        size = (long)Math.Pow(2, 10) * _size;
                    else if (_sizeMeasure == SizeMeasures.Mbyte)
                        size = (long)Math.Pow(2, 20) * _size;
                    else
                        size = (long)Math.Pow(2, 30) * _size;
                    if (_sign == Signs.Equal)
                    {
                        if (info.Length != size)
                            continue;
                    }
                    else if (_sign == Signs.Greater)
                    {
                        if (info.Length < size)
                            continue;
                    }
                    else
                    {
                        if (info.Length > size)
                            continue;
                    }
                }

                if (Attributes)
                {
                    FileAttributes attrs = File.GetAttributes(file);
                    if (ArchiveAttr != null)
                    {
                        if (ArchiveAttr == true && !attrs.HasFlag(FileAttributes.Archive) || ArchiveAttr == false && attrs.HasFlag(FileAttributes.Archive))
                            continue;
                    } 

                    if (SystemAttr != null)
                    {
                        if (SystemAttr == true && !attrs.HasFlag(FileAttributes.System) || SystemAttr == false && attrs.HasFlag(FileAttributes.System))
                            continue;
                    }

                    if (HiddenAttr != null)
                    {
                        if (HiddenAttr == true && !attrs.HasFlag(FileAttributes.Hidden) || HiddenAttr == false && attrs.HasFlag(FileAttributes.Hidden))
                            continue;
                    }

                    if (DirectoryAttr != null)
                    {
                        if (DirectoryAttr == true && !attrs.HasFlag(FileAttributes.Directory) || DirectoryAttr == false && attrs.HasFlag(FileAttributes.Directory))
                            continue;
                    }

                    if (EncryptedAttr != null)
                    {
                        if (EncryptedAttr == true && !attrs.HasFlag(FileAttributes.Encrypted) || EncryptedAttr == false && attrs.HasFlag(FileAttributes.Encrypted))
                            continue;
                    }

                    if (CompressedAttr != null)
                    {
                        if (CompressedAttr == true && !attrs.HasFlag(FileAttributes.Compressed) || CompressedAttr == false && attrs.HasFlag(FileAttributes.Compressed))
                            continue;
                    }

                    if (ReadOnlyAttr != null)
                    {
                        if (ReadOnlyAttr == true && !attrs.HasFlag(FileAttributes.ReadOnly) || ReadOnlyAttr == false && attrs.HasFlag(FileAttributes.ReadOnly))
                            continue;
                    }
                }
                filteredFiles.Add(file);
            }
            return filteredFiles;
        }
    }
}
