﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MyTotalCommander.Windows
{
    /// <summary>
    /// Interaction logic for TimePicker.xaml
    /// </summary>
    public partial class TimePicker : UserControl
    {
        public byte Seconds
        {
            get
            {
                byte secs;
                byte.TryParse(SecondsTB.Text.ToString(), out secs);
                return secs;
            }
        }

        public byte Minutes
        {
            get
            {
                byte mins;
                byte.TryParse(MinutesTB.Text.ToString(), out mins);
                return mins;
            }
        }

        public byte Hours
        {
            get
            {
                byte hours;
                byte.TryParse(HoursTB.Text.ToString(), out hours);
                return hours;
            }
        }

        public TimePicker()
        {
            InitializeComponent();
        }

        public void SetTime(DateTime time)
        {
            SecondsTB.Text = time.Second > 9 ? time.Second.ToString() : String.Format("0{0}", time.Second);
            MinutesTB.Text = time.Minute > 9 ? time.Minute.ToString() : String.Format("0{0}", time.Minute);
            HoursTB.Text = time.Hour > 9 ? time.Hour.ToString() : String.Format("0{0}", time.Hour);
        }

        private void Seconds_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key <= Key.D9 && e.Key >= Key.D0)
            {
                int input;
                int num;
                StringBuilder newText = new StringBuilder(SecondsTB.Text);
                int.TryParse(e.Key.ToString()[1].ToString(), out input);
                int.TryParse(SecondsTB.Text[1].ToString(), out num);
                if (SecondsTB.Text[0] == ' ' && num < 6)
                {
                    newText[0] = newText[1];
                    newText[1] = input.ToString()[0];
                    SecondsTB.Text = newText.ToString();

                }
                else
                {
                    newText[0] = ' ';
                    newText[1] = input.ToString()[0];
                    SecondsTB.Text = newText.ToString();
                }
                SecondsTB.SelectAll();
            }
        }

        private void Minutes_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key <= Key.D9 && e.Key >= Key.D0)
            {
                int input;
                int num;
                StringBuilder newText = new StringBuilder(MinutesTB.Text);
                int.TryParse(e.Key.ToString()[1].ToString(), out input);
                int.TryParse(MinutesTB.Text[1].ToString(), out num);
                if (MinutesTB.Text[0] == ' ' && num < 6)
                {
                    newText[0] = newText[1];
                    newText[1] = input.ToString()[0];
                    MinutesTB.Text = newText.ToString();

                }
                else
                {
                    newText[0] = ' ';
                    newText[1] = input.ToString()[0];
                    MinutesTB.Text = newText.ToString();
                }
                MinutesTB.SelectAll();
            }
        }

        private void Hours_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key <= Key.D9 && e.Key >= Key.D0)
            {
                int input;
                int num;
                StringBuilder newText = new StringBuilder(HoursTB.Text);
                int.TryParse(e.Key.ToString()[1].ToString(), out input);
                int.TryParse(HoursTB.Text[1].ToString(), out num);
                if (HoursTB.Text[0] == ' ' && (num == 2 && input < 4 || num < 2))
                {

                    newText[0] = newText[1];
                    newText[1] = input.ToString()[0];
                    HoursTB.Text = newText.ToString();

                }
                else
                {
                    newText[0] = ' ';
                    newText[1] = input.ToString()[0];
                    HoursTB.Text = newText.ToString();
                }
                HoursTB.SelectAll();
            }
        }

        private void Seconds_LostFocus(object sender, RoutedEventArgs e)
        {
            if (SecondsTB.Text[0] == ' ')
            {
                StringBuilder newText = new StringBuilder(SecondsTB.Text);
                newText[0] = '0';
                SecondsTB.Text = newText.ToString();
            }
        }

        private void Minutes_LostFocus(object sender, RoutedEventArgs e)
        {
            if (MinutesTB.Text[0] == ' ')
            {
                StringBuilder newText = new StringBuilder(MinutesTB.Text);
                newText[0] = '0';
                MinutesTB.Text = newText.ToString();
            }
        }

        private void Hours_LostFocus(object sender, RoutedEventArgs e)
        {
            if (HoursTB.Text[0] == ' ')
            {
                StringBuilder newText = new StringBuilder(HoursTB.Text);
                newText[0] = '0';
                HoursTB.Text = newText.ToString();
            }
        }

        private void UpBtn_Click(object sender, RoutedEventArgs e)
        {
            if (SecondsTB.IsSelectionActive)
            {
                int value;
                int.TryParse(SecondsTB.Text, out value);
                value++;
                if (value >= 60)
                    value = 0;
                StringBuilder newValue = new StringBuilder();
                if (value.ToString().Length == 2)
                    newValue.Append(value.ToString());
                else
                    newValue.AppendFormat("0{0}", value);
                SecondsTB.Text = newValue.ToString();
            }
            else if (MinutesTB.IsSelectionActive)
            {
                int value;
                int.TryParse(MinutesTB.Text, out value);
                value++;
                if (value >= 60)
                    value = 0;
                StringBuilder newValue = new StringBuilder();
                if (value.ToString().Length == 2)
                    newValue.Append(value.ToString());
                else
                    newValue.AppendFormat("0{0}", value);
                MinutesTB.Text = newValue.ToString();
            }
            else
            {
                HoursTB.Focus();
                int value;
                int.TryParse(HoursTB.Text, out value);
                value++;
                if (value >= 24)
                    value = 0;
                StringBuilder newValue = new StringBuilder();
                if (value.ToString().Length == 2)
                    newValue.Append(value.ToString());
                else
                    newValue.AppendFormat("0{0}", value);
                HoursTB.Text = newValue.ToString();
            }
        }

        private void DownBtn_Click(object sender, RoutedEventArgs e)
        {
            if (SecondsTB.IsSelectionActive)
            {
                int value;
                int.TryParse(SecondsTB.Text, out value);
                value--;
                if (value < 0)
                    value = 59;
                StringBuilder newValue = new StringBuilder();
                if (value.ToString().Length == 2)
                    newValue.Append(value.ToString());
                else
                    newValue.AppendFormat("0{0}", value);
                SecondsTB.Text = newValue.ToString();
            }
            else if (MinutesTB.IsSelectionActive)
            {
                int value;
                int.TryParse(MinutesTB.Text, out value);
                value++;
                if (value < 0)
                    value = 59;
                StringBuilder newValue = new StringBuilder();
                if (value.ToString().Length == 2)
                    newValue.Append(value.ToString());
                else
                    newValue.AppendFormat("0{0}", value);
                MinutesTB.Text = newValue.ToString();
            }
            else
            {
                HoursTB.Focus();
                int value;
                int.TryParse(HoursTB.Text, out value);
                value++;
                if (value < 0)
                    value = 23;
                StringBuilder newValue = new StringBuilder();
                if (value.ToString().Length == 2)
                    newValue.Append(value.ToString());
                else
                    newValue.AppendFormat("0{0}", value);
                HoursTB.Text = newValue.ToString();
            }
        }
    }
}
