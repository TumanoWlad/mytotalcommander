﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace MyTotalCommander
{
    /// <summary>
    /// Interaction logic for NewFolderWindow.xaml
    /// </summary>
    public partial class NewFolderWindow : Window
    {
        private string _path;
        public NewFolderWindow(string path)
        {
            InitializeComponent();
            _path = path;
            Header.Text += path;
        }

        private void CloseBtn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void OKBtn_Click(object sender, RoutedEventArgs e)
        {
            if (NewFolderNameTB.Text.Length != 0)
                Directory.CreateDirectory(_path += NewFolderNameTB.Text);
            Close();
        }
    }
}
