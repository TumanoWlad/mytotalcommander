﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using FileSystemClasses;

namespace MyTotalCommander
{
    /// <summary>
    /// Interaction logic for MoveWindow.xaml
    /// </summary>
    public partial class PreMoveWindow : Window
    {
        private string _path;
        private List<FSItemContent> _toMove;
        public PreMoveWindow(List<FSItemContent> toMove, string path)
        {
            InitializeComponent();
            _path = path;
            PathTB.Text = _path;
            _toMove = toMove;
            Header.Text = string.Format("Move {0} file(s) to", toMove.Count);
        }

        private void OKBtn_Click(object sender, RoutedEventArgs e)
        {
            _path = PathTB.Text;
            if (MainWindow.IsValidPath(_path))
            {
                if (_path.EndsWith("*.*"))
                    _path = _path.Remove(_path.LastIndexOf('*') - 2);
                if (System.IO.Path.IsPathRooted(_path) && _path[0] == '\\')
                    _path = System.IO.Path.Combine(System.IO.Path.GetPathRoot(_toMove[0].Path), _path.Remove(0, 1));
                else if (!System.IO.Path.IsPathRooted(_path))
                    _path = System.IO.Path.Combine(System.IO.Path.GetPathRoot(_toMove[0].Path), _path);
                MoveWindow window = new MoveWindow(_toMove, _path);
                window.Show();
                Close();
            }
            else
            {
                MessageBox.Show("Invalid Destintaion Path!");
            }
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
