﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using FileSystemClasses;
using System.IO;

namespace MyTotalCommander
{
    /// <summary>
    /// Interaction logic for DeleteWindow.xaml
    /// </summary>
    public partial class DeleteWindow : Window
    {
        private DispatcherTimer _timer;
        private List<FSItemContent> _toDelete;
        private FSItemContent _currentDeleteingItem;
        private int _total;
        private int _currentIndex;
        double _percentsPerItem;
        public DeleteWindow(List<FSItemContent> toDelete)
        {
            InitializeComponent();
            _toDelete = toDelete;
            _timer = new DispatcherTimer();
            _timer.Tick += dispatcherTimer_Tick;
            _timer.Interval = new TimeSpan(0, 0, 0, 0, 1);
            _timer.Start();
            _total = _toDelete.Count;
            _currentIndex = 0;
            _percentsPerItem = 100.0 / _total;
            DeleteProgress.Value = 0;
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            _timer.Stop();
            _currentDeleteingItem = _toDelete[_currentIndex++];
            CurrentItem.Text = string.Format("-> Deleting: {0}", _currentDeleteingItem.Name);
            try
            {
                _currentDeleteingItem.Delete();
            }
            catch (UnauthorizedAccessException ex)
            {
                MessageBoxResult result = MessageBox.Show(ex.Message + "\nContinue deleting rest items?", "My Total Commander", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.No)
                    Close();
            }
            catch(OperationCanceledException ex)
            {
                MessageBoxResult result = MessageBox.Show(ex.Message + "\nContinue deleting?", "My Total Commander", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.No)
                    Close();
            }
            catch (IOException)
            {
                string message = string.Format("The directory {0} is not empty!\nDo you want to delete it with all it's files and subdirectories?", _currentDeleteingItem.Path);
                MessageBoxResult result = MessageBox.Show(message, "My Total Commander", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                    _currentDeleteingItem.Delete();
                else if (result == MessageBoxResult.Cancel)
                    Close();
            }
            DeleteProgress.Value += _percentsPerItem;
            Percents.Text = string.Format("{0}%", DeleteProgress.Value);
            if (_currentIndex >= _total)
                Close();
            else
                _timer.Start();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _timer.Stop();
        }

        private void PauseBtn_Click(object sender, RoutedEventArgs e)
        {
            if (_timer.IsEnabled)
            {
                _timer.Stop();
                PauseBtn.Content = "Start";
            }
            else
            {
                _timer.Start();
                PauseBtn.Content = "Stop";
            }
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            _timer.Stop();
            MessageBoxResult result = MessageBox.Show("User abort!", "My Total Commander", MessageBoxButton.OKCancel, MessageBoxImage.Stop);
            if (result == MessageBoxResult.OK)
                Close();
            else
                _timer.Start();
        }


    }
}
