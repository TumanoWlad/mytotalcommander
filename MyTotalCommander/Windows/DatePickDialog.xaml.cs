﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MyTotalCommander
{
    /// <summary>
    /// Interaction logic for DatePickDialog.xaml
    /// </summary>
    public partial class DatePickDialog : Window
    {
        private DateTime _date;
        public DateTime Date
        {
            get
            {
                return _date;
            }
            private set
            {
                _date = value;
            }
        }

        public DatePickDialog()
        {
            InitializeComponent();
            _date = DateTime.Now;
            datePicker.SelectedDate = _date;
            timePicker.SetTime(DateTime.Now);
        }

        private void Cancel_Btn_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void OK_Btn_Click(object sender, RoutedEventArgs e)
        {
            _date = new DateTime(datePicker.SelectedDate.Value.Year, datePicker.SelectedDate.Value.Month, datePicker.SelectedDate.Value.Day, timePicker.Hours, timePicker.Minutes, timePicker.Seconds);
            DialogResult = true;
            Close();
        }


    }
}
