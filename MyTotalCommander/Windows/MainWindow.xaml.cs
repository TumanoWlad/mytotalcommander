﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FileSystemClasses;
using System.IO;
using System.Diagnostics;

namespace MyTotalCommander
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Button _leftSelectedButton, _rightSelectedButton;
        private CommanderViewModel _rightViewModel, _leftViewModel;
        private DataGridRow _currentItem;
        private List<DataGridRow> rightSelectedItems;
        private List<DataGridRow> leftSelectedItems;
        private bool _mouseSelectionMode;
        private FileSystemWatcher _leftPathWatcher;
        private FileSystemWatcher _rightPathWatcher;

        public MainWindow()
        {
            InitializeComponent();
            _rightViewModel = new CommanderViewModel(1);
            _leftViewModel = new CommanderViewModel(0);
            rightSelectedItems = new List<DataGridRow>();
            leftSelectedItems = new List<DataGridRow>();
            _currentItem = null;
            LeftWorkArea.DataContext = _leftViewModel.Items;
            RightWorkArea.DataContext = _rightViewModel.Items;
            LeftDriveButtonPanel.ItemsSource = _leftViewModel.drives;
            RightDriveButtonPanel.ItemsSource = _rightViewModel.drives;
            LeftPath.DataContext = _leftViewModel;
            RightPath.DataContext = _rightViewModel;
            SetPathWatchers();
        }

        private void SetPathWatchers()
        {
            RefreshLeftWatcher();
            RefreshRightWatcher();
        }

        private void RefreshLeftWatcher()
        {
            _leftPathWatcher = new FileSystemWatcher(_leftViewModel.Path.Remove(_leftViewModel.Path.LastIndexOf('\\') + 1));
            _leftPathWatcher.EnableRaisingEvents = true;
            _leftPathWatcher.Created += OnLeftChanged;
            _leftPathWatcher.Deleted += OnLeftChanged;
            _leftPathWatcher.Changed += OnLeftChanged;
        }

        private void RefreshRightWatcher()
        {
            _rightPathWatcher = new FileSystemWatcher(_rightViewModel.Path.Remove(_rightViewModel.Path.LastIndexOf('\\') + 1));
            _rightPathWatcher.EnableRaisingEvents = true;
            _rightPathWatcher.Created += OnRightChanged;
            _rightPathWatcher.Deleted += OnRightChanged;
            _rightPathWatcher.Changed += OnRightChanged;
        }

        private void OnRightChanged(object sender, FileSystemEventArgs e)
        {
            _rightViewModel.RefreshItems();
            Dispatcher.BeginInvoke(new Action(() => RightWorkArea.DataContext = _rightViewModel.Items));
            rightSelectedItems.Clear();
        }

        private void OnLeftChanged(object sender, FileSystemEventArgs e)
        {
            _leftViewModel.RefreshItems();
            Dispatcher.BeginInvoke(new Action(() => LeftWorkArea.DataContext = _leftViewModel.Items));
            leftSelectedItems.Clear();
        }

        private void Total_Commander_Loaded(object sender, RoutedEventArgs e)
        {
            var container = LeftDriveButtonPanel.ItemContainerGenerator.ContainerFromItem(_leftViewModel.drives.Find(drive =>
                drive.Letter.ToString().ToUpper() == _leftViewModel.Path[0].ToString())) as ContentPresenter;
            _leftSelectedButton = LeftDriveButtonPanel.ItemTemplate.FindName("DB", container) as Button;

            container = RightDriveButtonPanel.ItemContainerGenerator.ContainerFromItem(_rightViewModel.drives.Find(drive =>
                drive.Letter.ToString().ToUpper() == _rightViewModel.Path[0].ToString())) as ContentPresenter;
            _rightSelectedButton = RightDriveButtonPanel.ItemTemplate.FindName("DB", container) as Button;

            _leftSelectedButton.Style = _rightSelectedButton.Style = (Style)this.FindResource("SelectedDriveButton");
            if (RightWorkArea.CurrentCell.Column == null)
            {
                MarkableItem<FSItemContent> gridItem = RightWorkArea.Items[0] as MarkableItem<FSItemContent>;
                gridItem.IsSelected = true;
            }
        }

        public void Item_DoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                MarkableItem<FSItemContent> clickedItem = ((DataGridRow)sender).DataContext as MarkableItem<FSItemContent>;
                if (LeftWorkArea.Items.Contains(clickedItem))
                {
                    _leftViewModel.Open(clickedItem.Item);
                    LeftWorkArea.DataContext = _leftViewModel.Items;
                    leftSelectedItems.Clear();
                    _leftPathWatcher.Path = _leftViewModel.Path.Remove(_leftViewModel.Path.LastIndexOf('\\') + 1);
                }
                else
                {
                    _rightViewModel.Open(clickedItem.Item);
                    RightWorkArea.DataContext = _rightViewModel.Items;
                    rightSelectedItems.Clear();
                    _rightPathWatcher.Path = _rightViewModel.Path.Remove(_rightViewModel.Path.LastIndexOf('\\') + 1);
                }
            }
        }

        public void Item_RightBtnDown(object sender, MouseButtonEventArgs e)
        {
            DataGridRow row = sender as DataGridRow;
            row.IsSelected = true;
            if (LeftWorkArea.Items.Contains(row.DataContext))
            {
                if (leftSelectedItems.Contains(row))
                {
                    ((MarkableItem<FSItemContent>)row.DataContext).IsMarked = false;
                    leftSelectedItems.Remove(row);
                    _mouseSelectionMode = false;
                }
                else
                {
                    leftSelectedItems.Add(row);
                    ((MarkableItem<FSItemContent>)row.DataContext).IsMarked = true;
                    _mouseSelectionMode = true;
                }
            }
            else 
            {
                if (rightSelectedItems.Contains(row))
                {
                    ((MarkableItem<FSItemContent>)row.DataContext).IsMarked = false;
                    rightSelectedItems.Remove(row);
                    _mouseSelectionMode = false;
                }
                else
                {
                    rightSelectedItems.Add(row);
                    ((MarkableItem<FSItemContent>)row.DataContext).IsMarked = true;
                    _mouseSelectionMode = true;
                }
            } 
        }

        void Row_MouseEntered(object sender, MouseEventArgs e)
        {
            if (e.RightButton == MouseButtonState.Pressed)
            {
                DataGridRow row = sender as DataGridRow;
                row.IsSelected = true;
                if (LeftWorkArea.Items.Contains(row.DataContext))
                {
                    if (leftSelectedItems.Contains(row) && _mouseSelectionMode == false)
                    {
                        ((MarkableItem<FSItemContent>)row.DataContext).IsMarked = false;
                        leftSelectedItems.Remove(row);
                    }
                    else if (!leftSelectedItems.Contains(row) && _mouseSelectionMode == true)
                    {
                        leftSelectedItems.Add(row);
                        ((MarkableItem<FSItemContent>)row.DataContext).IsMarked = true;
                    }
                }
                else
                {
                    if (rightSelectedItems.Contains(row) && _mouseSelectionMode == false)
                    {
                        ((MarkableItem<FSItemContent>)row.DataContext).IsMarked = false;
                        rightSelectedItems.Remove(row);
                    }
                    else if (!rightSelectedItems.Contains(row) && _mouseSelectionMode == true)
                    {
                        rightSelectedItems.Add(row);
                        ((MarkableItem<FSItemContent>)row.DataContext).IsMarked = true;
                    }
                } 
            }
        }

        public void Item_Selected(object sender, RoutedEventArgs e)
        {
            StringBuilder newCommandPath = new StringBuilder();
            if (_currentItem != null)
                _currentItem.IsSelected = false;
            _currentItem = sender as DataGridRow;
            if (LeftWorkArea.Items.Contains(_currentItem.DataContext))
                newCommandPath.Append(LeftPath.Content as string);
            else
                newCommandPath.Append(RightPath.Content as string);
            newCommandPath.Replace("*.*", ">");
            CommandPath.Text = newCommandPath.ToString();
        }

        // Command handlers
        private void DriveButtonSelection_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Button clicked = sender as Button;
            if (clicked.Style != (Style)this.FindResource("SelectedDriveButton"))
            {
                try
                {
                    if (LeftDriveButtonPanel.Items.Contains(clicked.DataContext))
                    {
                        _leftViewModel.SetDrive(clicked.DataContext as Drive);
                        LeftWorkArea.DataContext = _leftViewModel.Items;
                        _leftSelectedButton.Style = (Style)this.FindResource("DriveButton");
                        _leftSelectedButton = clicked;
                        _leftSelectedButton.Style = (Style)this.FindResource("SelectedDriveButton");
                        RefreshLeftWatcher();
                        leftSelectedItems.Clear();
                    }
                    else
                    {
                        _rightViewModel.SetDrive((Drive)clicked.DataContext);
                        RightWorkArea.DataContext = _rightViewModel.Items;
                        _rightSelectedButton.Style = (Style)this.FindResource("DriveButton");
                        _rightSelectedButton = clicked;
                        _rightSelectedButton.Style = (Style)this.FindResource("SelectedDriveButton");
                        RefreshRightWatcher();
                        rightSelectedItems.Clear();
                    }
                }
                catch (IOException)
                {
                    MessageBox.Show("Can't get access to drive " + ((Button)sender).Content);
                }
            }
        }

        private void Edit_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (_currentItem != null && _currentItem.DataContext != null)
            {
                FileContent item = ((MarkableItem<FSItemContent>)_currentItem.DataContext).Item as FileContent;
                if (item != null)
                    CommanderViewModel.Edit(item.Path);
                else
                    MessageBox.Show("No files selected!");
            }
        }

        private void Closing_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.Close();
        }

        private void NewFolder_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (_currentItem != null && _currentItem.DataContext != null)
            {
                NewFolderWindow dialog;
                if (LeftWorkArea.Items.Contains(_currentItem.DataContext))
                {
                    dialog = new NewFolderWindow(_leftViewModel.Path.Remove(_leftViewModel.Path.LastIndexOf('\\') + 1));
                }
                else
                    dialog = new NewFolderWindow(_rightViewModel.Path.Remove(_rightViewModel.Path.LastIndexOf('\\') + 1));
                dialog.ShowDialog();
            }
        }

        private void Delete_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (_currentItem != null && _currentItem.DataContext != null)
            {
                List<FSItemContent> toDelete = new List<FSItemContent>();
                DeleteWindow newWindow;
                if (LeftWorkArea.Items.Contains(_currentItem.DataContext) && leftSelectedItems.Count != 0)
                    foreach (var item in leftSelectedItems)
                        toDelete.Add(((MarkableItem<FSItemContent>)item.DataContext).Item);
                else if (RightWorkArea.Items.Contains(_currentItem.DataContext) && rightSelectedItems.Count != 0)
                    foreach (var item in rightSelectedItems)
                        toDelete.Add(((MarkableItem<FSItemContent>)item.DataContext).Item);
                else
                    toDelete.Add(((MarkableItem<FSItemContent>)_currentItem.DataContext).Item);
                if (toDelete.Count < 5)
                {
                    string messageBoxText = String.Format("Do you really want to delete following item(s)?");
                    foreach (var item in toDelete)
                        messageBoxText += ("\n" + item.Name);
                    string caption = "My Total Commander";
                    MessageBoxButton button = MessageBoxButton.YesNoCancel;
                    MessageBoxImage icon = MessageBoxImage.Question;
                    MessageBoxResult result = MessageBox.Show(messageBoxText, caption, button, icon);
                    switch (result)
                    {
                        case MessageBoxResult.Yes:
                            newWindow = new DeleteWindow(toDelete);
                            newWindow.Show();
                            break;
                        case MessageBoxResult.Cancel:
                        case MessageBoxResult.No:
                            break;
                    }
                }
                else
                {
                    newWindow = new DeleteWindow(toDelete);
                    newWindow.Show();
                }
            }
        }

        private void Copy_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (_currentItem != null &&_currentItem.DataContext != null)
            {
                List<FSItemContent> toCopy = new List<FSItemContent>();
                string pathTo = "";

                if (LeftWorkArea.Items.Contains(_currentItem.DataContext))
                {
                    if (leftSelectedItems.Count != 0)
                        foreach (var item in leftSelectedItems)
                            toCopy.Add(((MarkableItem<FSItemContent>)item.DataContext).Item);
                    else
                        toCopy.Add(((MarkableItem<FSItemContent>)_currentItem.DataContext).Item);
                    pathTo = RightPath.Content as string;
                }
                else
                {
                    if (rightSelectedItems.Count != 0)
                        foreach (var item in rightSelectedItems)
                            toCopy.Add(((MarkableItem<FSItemContent>)item.DataContext).Item);
                    else
                        toCopy.Add(((MarkableItem<FSItemContent>)_currentItem.DataContext).Item);
                    pathTo = LeftPath.Content as string;
                }

                PreCopyWindow dialog = new PreCopyWindow(toCopy, pathTo);
                dialog.ShowDialog();
            }
        }

        private void Move_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (_currentItem != null && _currentItem.DataContext != null)
            {
                List<FSItemContent> toCopy = new List<FSItemContent>();
                string pathTo = "";

                if (LeftWorkArea.Items.Contains(_currentItem.DataContext))
                {
                    if (leftSelectedItems.Count != 0)
                        foreach (var item in leftSelectedItems)
                            toCopy.Add(((MarkableItem<FSItemContent>)item.DataContext).Item);
                    else
                        toCopy.Add(((MarkableItem<FSItemContent>)_currentItem.DataContext).Item);
                    pathTo = RightPath.Content as string;
                }
                else
                {
                    if (rightSelectedItems.Count != 0)
                        foreach (var item in rightSelectedItems)
                            toCopy.Add(((MarkableItem<FSItemContent>)item.DataContext).Item);
                    else
                        toCopy.Add(((MarkableItem<FSItemContent>)_currentItem.DataContext).Item);
                    pathTo = LeftPath.Content as string;
                }

                PreMoveWindow dialog = new PreMoveWindow(toCopy, pathTo);
                dialog.ShowDialog();
            }
        }

        private void Search_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            string path = "";
            if (_currentItem != null && LeftWorkArea.Items.Contains(_currentItem.DataContext))
                path = LeftPath.Content as string;
            else
                path = RightPath.Content as string;
            SearchWindow window = new SearchWindow(path.Remove(path.LastIndexOf('.') - 1));
            window.ShowDialog();
            if (window.DialogResult == true)
            {
                if (LeftWorkArea.Items.Contains(_currentItem.DataContext))
                    LeftAreaGoToFile(window.ReturnPath);
                else
                    RightAreaGoToFile(window.ReturnPath);
            }
        }

        public static bool IsValidPath(string path)
        {
            char[] vrongs = System.IO.Path.GetInvalidPathChars();
            foreach (char c in vrongs)
                if (path.Contains(c))
                    return false;
            return true;
        }

        private void RightPath_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ((Label)sender).Visibility = System.Windows.Visibility.Collapsed;
            string currentPath = ((Label)sender).Content as string;
            currentPath = currentPath.Remove(currentPath.Length - 3);
            RightPathBox.Text = currentPath;
            RightPathBox.Visibility = System.Windows.Visibility.Visible;
            RightGoToBtn.Visibility = System.Windows.Visibility.Visible;
            RightPathBox.Focus();
            RightPathBox.SelectAll();
        }

        private void LeftPath_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ((Label)sender).Visibility = System.Windows.Visibility.Collapsed;
            string currentPath = ((Label)sender).Content as string;
            currentPath = currentPath.Remove(currentPath.Length - 3);
            LeftPathBox.Text = currentPath;
            LeftPathBox.Visibility = System.Windows.Visibility.Visible;
            LeftGoToBtn.Visibility = System.Windows.Visibility.Visible;
            LeftPathBox.Focus();
            LeftPathBox.SelectAll();
        }

        private void RightGoToBtn_Click(object sender, RoutedEventArgs e)
        {
            RightAreaGoToFile(RightPathBox.Text);
            RightPathBox.Visibility = System.Windows.Visibility.Collapsed;
            RightGoToBtn.Visibility = System.Windows.Visibility.Collapsed;
            RightPath.Visibility = System.Windows.Visibility.Visible;
        }

        private void LeftGoToBtn_Click(object sender, RoutedEventArgs e)
        {
            LeftAreaGoToFile(LeftPathBox.Text);
            LeftPathBox.Visibility = System.Windows.Visibility.Collapsed;
            LeftGoToBtn.Visibility = System.Windows.Visibility.Collapsed;
            LeftPath.Visibility = System.Windows.Visibility.Visible;
        }

        private void RightPathBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                RightAreaGoToFile(RightPathBox.Text);
                RightPathBox.Visibility = System.Windows.Visibility.Collapsed;
                RightGoToBtn.Visibility = System.Windows.Visibility.Collapsed;
                RightPath.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void LeftPathBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                LeftAreaGoToFile(LeftPathBox.Text);
                LeftPathBox.Visibility = System.Windows.Visibility.Collapsed;
                LeftGoToBtn.Visibility = System.Windows.Visibility.Collapsed;
                LeftPath.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void Total_Commander_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (RightPathBox.IsVisible && !RightPathBox.IsMouseOver && !RightGoToBtn.IsMouseOver)
            {
                RightPathBox.Visibility = System.Windows.Visibility.Collapsed;
                RightGoToBtn.Visibility = System.Windows.Visibility.Collapsed;
                RightPath.Visibility = System.Windows.Visibility.Visible;
            }
            else if (LeftPathBox.IsVisible && !LeftPathBox.IsMouseOver && !LeftGoToBtn.IsMouseOver)
            {
                LeftPathBox.Visibility = Visibility.Collapsed;
                LeftGoToBtn.Visibility = Visibility.Collapsed;
                LeftPath.Visibility = Visibility.Visible;
            }
        }

        private void CommandRow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    ProcessStartInfo procStartInfo = new ProcessStartInfo("cmd", "/c " + CommandRow.Text);
                    procStartInfo.WorkingDirectory = CommandPath.Text.Remove(CommandPath.Text.Length - 1);
                    procStartInfo.RedirectStandardOutput = true;
                    procStartInfo.UseShellExecute = false;
                    // Do not create the black window.
                    procStartInfo.CreateNoWindow = true;
                    // Now we create a process, assign its ProcessStartInfo and start it
                    Process proc = new Process();
                    proc.StartInfo = procStartInfo;
                    proc.Start();
                    string s = proc.StandardError.ReadToEnd();
                }
                catch (Exception)
                {
                    // Log the exception
                }

            }
        }

        private void LeftAreaGoToFile(string filename)
        {
            if (filename[filename.Length - 1] == '\\')
                filename = filename.Remove(filename.Length - 1);
            if (File.Exists(filename))
            {
                if ((((string)LeftPath.Content)[0] != filename.ToUpper()[0] || filename.Length <= 3) && Drive.Exists(filename.Substring(0, 2)))
                {
                    Drive chosenDrive = _leftViewModel.drives.Find(item => item.Letter == filename.ToLower()[0]);
                    var container = LeftDriveButtonPanel.ItemContainerGenerator.ContainerFromItem(chosenDrive) as ContentPresenter;
                    Button driveButton = LeftDriveButtonPanel.ItemTemplate.FindName("DB", container) as Button;
                    try
                    {
                        _leftViewModel.SetDrive((Drive)driveButton.DataContext);
                        _leftSelectedButton.Style = (Style)this.FindResource("DriveButton");
                        _leftSelectedButton = driveButton;
                        _leftSelectedButton.Style = (Style)this.FindResource("SelectedDriveButton");
                    }
                    catch (IOException)
                    {
                        MessageBox.Show("Can't get access to drive " + chosenDrive.Letter.ToString());
                    }
                }
                if (filename.Length <= 3)
                    LeftWorkArea.DataContext = _leftViewModel.Items;
                else
                {
                    _leftViewModel.Open(FolderContent.GetFolderFromPath(filename));
                    LeftWorkArea.DataContext = _leftViewModel.Items;
                    leftSelectedItems.Clear();
                    _leftPathWatcher.Path = _leftViewModel.Path.Remove(_leftViewModel.Path.LastIndexOf('\\') + 1);
                }
                FileAttributes attributes = File.GetAttributes(filename);
                if (!attributes.HasFlag(FileAttributes.Directory))
                {
                    foreach (MarkableItem<FSItemContent> gridItem in LeftWorkArea.Items)
                    {
                        if (gridItem.Item.Path.ToLower() == filename.ToLower())
                        {
                            gridItem.IsSelected = true;
                            break;
                        }
                    }
                }
                RefreshLeftWatcher();
                leftSelectedItems.Clear();
            }
        }

        private void RightAreaGoToFile(string filename)
        {
            if (filename[filename.Length - 1] == '\\')
                filename = filename.Remove(filename.Length - 1);
            if (File.Exists(filename))
            {
                if ((((string)RightPath.Content)[0] != filename.ToUpper()[0] || filename.Length <= 3) && Drive.Exists(filename.Substring(0, 2)))
                {
                    Drive chosenDrive = _rightViewModel.drives.Find(item => item.Letter == filename.ToLower()[0]);
                    var container = RightDriveButtonPanel.ItemContainerGenerator.ContainerFromItem(chosenDrive) as ContentPresenter;
                    Button driveButton = RightDriveButtonPanel.ItemTemplate.FindName("DB", container) as Button;
                    try
                    {
                        _rightViewModel.SetDrive((Drive)driveButton.DataContext);
                        _rightSelectedButton.Style = (Style)this.FindResource("DriveButton");
                        _rightSelectedButton = driveButton;
                        _rightSelectedButton.Style = (Style)this.FindResource("SelectedDriveButton");
                    }
                    catch (IOException)
                    {
                        MessageBox.Show("Can't get access to drive " + chosenDrive.Letter.ToString());
                    }
                }
                if (filename.Length <= 3)
                    RightWorkArea.DataContext = _rightViewModel.Items;
                else
                {
                    _rightViewModel.Open(FolderContent.GetFolderFromPath(filename));
                    RightWorkArea.DataContext = _rightViewModel.Items;
                    rightSelectedItems.Clear();
                    _rightPathWatcher.Path = _rightViewModel.Path.Remove(_rightViewModel.Path.LastIndexOf('\\') + 1);
                }

                FileAttributes attributes = File.GetAttributes(filename);
                if (!attributes.HasFlag(FileAttributes.Directory))
                {
                    foreach (MarkableItem<FSItemContent> gridItem in RightWorkArea.Items)
                    {
                        if (gridItem.Item.Path.ToLower() == filename.ToLower())
                        {
                            gridItem.IsSelected = true;
                            break;
                        }
                    }
                }
                RefreshRightWatcher();
                rightSelectedItems.Clear();
            }
        }
    }
}
