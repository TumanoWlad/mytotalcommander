﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace MyTotalCommander
{
    /// <summary>
    /// Interaction logic for SearchWindow.xaml
    /// </summary>
    public partial class SearchWindow : Window
    {
        private static List<string> levels;
        private static List<string> timeMeasures;
        private static List<string> compareSigns;
        private static List<string> sizeMeasures;

        private Task searchTask;
        public string ReturnPath
        {
            get;
            private set;
        }

        static SearchWindow()
        {
            GenerateLevels();
            GenerateTimeMeasures();
            GenerateSigns();
            GenerateSizeMeasures();
        }

        private static void GenerateLevels()
        {
            levels = new List<string>();
            levels.Add("all(unlimited depth)");
            levels.Add("current dir only");
            for (int i = 1; i <= 100; i++)
                levels.Add(string.Format("{0} level(s)", i));
        }

        private static void GenerateTimeMeasures()
        {
            timeMeasures = new List<string>();
            timeMeasures.Add("Minute(s)");
            timeMeasures.Add("Hour(s)");
            timeMeasures.Add("Day(s)");
            timeMeasures.Add("Week(s)");
            timeMeasures.Add("Month(s)");
            timeMeasures.Add("Year(s)");
        }

        private static void GenerateSigns()
        {
            compareSigns = new List<string>();
            compareSigns.Add("=");
            compareSigns.Add(">");
            compareSigns.Add("<");
        }

        private static void GenerateSizeMeasures()
        {
            sizeMeasures = new List<string>();
            sizeMeasures.Add("bytes");
            sizeMeasures.Add("Kbytes");
            sizeMeasures.Add("Mbytes");
            sizeMeasures.Add("Gbytes");
        }

        public SearchWindow(string path)
        {
            InitializeComponent();

            ANSI_CB.IsChecked = true;
            DataContext = SearchViewModel.Instance;
            LevelBox.ItemsSource = levels;
            PeriodCB.ItemsSource = timeMeasures;
            MeasureCB.ItemsSource = sizeMeasures;
            SignBox.ItemsSource = compareSigns;
            SearchInTB.Text = path;
            SearchViewModel.Instance.SearchPath = path;
            searchTask = null;
            DateToTB.Text = DateTime.Now.ToShortDateString();
            DateFromTB.Text = "";
            ArchiveCB.IsChecked = null;
            SystemCB.IsChecked = null;
            HiddenCB.IsChecked = null;
            CompressedCB.IsChecked = null;
            EncryptedCB.IsChecked = null;
            ReadOnlyCB.IsChecked = null;
            DirectoryCB.IsChecked = null;
            DateTB.Text = SizeTB.Text = "1";
        }

        private void BrowseBtn_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog();
            folderBrowser.ShowDialog();
            SearchInTB.Text = folderBrowser.SelectedPath;
            SearchViewModel.Instance.SearchPath = folderBrowser.SelectedPath;
        }

        private void SearchBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!CheckAndCancelSearch())
            {
                if (SearchForTB.Text == "")
                    SearchViewModel.Instance.SearchPattern = "*.*";
                else if (!(MainWindow.IsValidPath(SearchForTB.Text)) && !((bool)RegExCB.IsChecked))
                {
                    System.Windows.MessageBox.Show("Vrong characters in search pattern!");
                    return;
                }
                else if ((bool)RegExCB.IsChecked)
                {
                    try
                    {
                        Regex expression = new Regex(SearchForTB.Text, RegexOptions.IgnoreCase);
                    }
                    catch(Exception)
                    {
                        System.Windows.MessageBox.Show("Search pattern is not a regular expression!");
                        return;
                    }
                }

                if (SearchInTB.Text == "")
                    SearchViewModel.Instance.SearchPath = "*";
                else if (!(MainWindow.IsValidPath(SearchInTB.Text)))
                {
                    System.Windows.MessageBox.Show("Vrong characters in path!");
                    return;
                }
                else if (!(Directory.Exists(SearchInTB.Text)))
                {
                    System.Windows.MessageBox.Show(String.Format("Path does not exist in this file system: {0}!", SearchInTB.Text));
                    return;
                }

                string levelStr = LevelBox.Text;
                switch (levelStr)
                {
                    case "all(unlimited depth)":
                        SearchViewModel.Instance.Level = Int32.MaxValue;
                        break;
                    case "current dir only":
                        SearchViewModel.Instance.Level = 0;
                        break;
                    default:
                        SearchViewModel.Instance.Level = int.Parse(levelStr.Split(' ')[0]);
                        break;
                }
                SearchBtn.Content = "Stop";
                NewSearchBtn.IsEnabled = false;
                ToFileBtn.IsEnabled = false;
                searchTask = Task.Run(() => SearchViewModel.Instance.Search());
                searchTask.ContinueWith(_ => { Dispatcher.Invoke(() => {
                    SearchBtn.Content = "Start search";
                    NewSearchBtn.IsEnabled = true;
                    ToFileBtn.IsEnabled = true;
                }); });
                
                this.MaxHeight = 10000;
                this.MinHeight = 310;
                this.Height = 500;
                ResultViev.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            if (!CheckAndCancelSearch())
                Close();
        }

        private void NewSearchBtn_Click(object sender, RoutedEventArgs e)
        {
            CheckAndCancelSearch();
            ResultViev.Visibility = System.Windows.Visibility.Collapsed;
            MaxHeight = MinHeight = 265;
            SearchForTB.Focus();
            SearchForTB.SelectAll();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (searchTask != null && !(searchTask.IsCompleted))
                SearchViewModel.Instance.CancelSearch();
        }

        private bool CheckAndCancelSearch()
        {
            if (searchTask != null && !(searchTask.IsCompleted))
            {
                SearchViewModel.Instance.CancelSearch();
                return true;
            }
            return false;
        }

        private void Encoding_Unchecked(object sender, RoutedEventArgs e)
        {
            if (!(bool)ANSI_CB.IsChecked && !(bool)ASCII_CB.IsChecked && !(bool)UTF16_CB.IsChecked && !(bool)UTF8_CB.IsChecked)
            {
                ANSI_CB.IsChecked = true;
            }
        }

        private void DateFromBtn_Click(object sender, RoutedEventArgs e)
        {
            DatePickDialog dateDialog = new DatePickDialog();
            dateDialog.ShowDialog();
            if (dateDialog.DialogResult.HasValue && dateDialog.DialogResult.Value == true)
                 SearchViewModel.Instance.DateFrom = dateDialog.Date;
        }

        private void DateToBtn_Click(object sender, RoutedEventArgs e)
        {
            DatePickDialog dateDialog = new DatePickDialog();
            dateDialog.ShowDialog();
            if (dateDialog.DialogResult.HasValue && dateDialog.DialogResult.Value == true)
                SearchViewModel.Instance.DateTo = dateDialog.Date;
        }

        private void SB_Increase(object sender, RoutedEventArgs e)
        {
            if (DateScroll.IsMouseOver)
            {
                int dateSpan;
                bool parsed = int.TryParse(DateTB.Text, out dateSpan);
                if (parsed)
                    dateSpan++;
                else
                    dateSpan = 0;
                DateTB.Text = dateSpan.ToString();
            }
            else if (SizeScroll.IsMouseOver)
            {
                int size;
                bool parsed = int.TryParse(SizeTB.Text, out size);
                if (parsed)
                    size++;
                else
                    size = 0;
                SizeTB.Text = size.ToString();
            }
        }

        private void SB_Decrease(object sender, RoutedEventArgs e)
        {
            if (DateScroll.IsMouseOver)
            {
                int dateSpan;
                bool parsed = int.TryParse(DateTB.Text, out dateSpan);
                if (parsed && dateSpan > 0)
                    dateSpan--;
                else
                    dateSpan = 0;
                DateTB.Text = dateSpan.ToString();
            }
            else if (SizeScroll.IsMouseOver)
            {
                int size;
                bool parsed = int.TryParse(SizeTB.Text, out size);
                if (parsed && size > 0)
                    size--;
                else
                    size = 0;
                SizeTB.Text = size.ToString();
            }
        }

        private void DateCB_Checked(object sender, RoutedEventArgs e)
        {
            if (NotOlderCB.IsChecked == true)
                NotOlderCB.IsChecked = false;

        }

        private void NotOlderCB_Checked(object sender, RoutedEventArgs e)
        {
            if (DateCB.IsChecked == true)
                DateCB.IsChecked = false;
        }

        private void FinishDialogOK()
        {
            DialogResult = true;
            ReturnPath = ResultList.SelectedItem.ToString();
            Close();
        }

        private void ToFileBtn_Click(object sender, RoutedEventArgs e)
        {
            FinishDialogOK();
        }

        private void ResultList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            FinishDialogOK();
        }
    }
}
