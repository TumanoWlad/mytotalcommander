﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;
using FileSystemClasses;
using System.IO;

namespace MyTotalCommander
{
    /// <summary>
    /// Interaction logic for MoveWindow.xaml
    /// </summary>
    public partial class MoveWindow : Window
    {
        private string _path;
        private int _total;
        private int _currentIndex;
        private double _percentsPerItem;
        private Task _task;
        private CancellationTokenSource _tokenSource;
        private CancellationToken _token;

        public MoveWindow(List<FSItemContent> moveContent, string path)
        {
            InitializeComponent();
            _path = path;
            _tokenSource = new CancellationTokenSource();
            _token = _tokenSource.Token;
            _total = moveContent.Count;
            _currentIndex = 0;
            _percentsPerItem = 100.0 / _total;
            CopyProgress.Value = 0;
            _task = Task.Factory.StartNew(() => Move(moveContent, _token), _token);
        }

        private void Move(List<FSItemContent> moveContent, CancellationToken token)
        {
            FSItemContent _currentMoveingItem;
            while (_currentIndex < _total)
            {
                _currentMoveingItem = moveContent[_currentIndex++];
                token.ThrowIfCancellationRequested();
                Dispatcher.BeginInvoke(new Action(() => UpdateLable(_currentMoveingItem.Path, System.IO.Path.Combine(_path, _currentMoveingItem.Name))));
                if (token.IsCancellationRequested)
                    break;
                try
                {
                    _currentMoveingItem.Move(_path);
                }
                catch (UnauthorizedAccessException ex)
                {
                    MessageBoxResult result = MessageBox.Show(ex.Message);
                    break;
                }
                catch(DirectoryNotFoundException)
                {
                    MessageBox.Show("You can't copy or move more than one file\nto a single file!", "My Total Commander", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    break;
                }
                catch (IOException ex)
                {
                    if (_currentIndex == _total - 1)
                    {
                        MessageBoxResult result = MessageBox.Show(ex.Message + "\nContinue and try to move rest items?", "My Total Commander", MessageBoxButton.YesNo, MessageBoxImage.Question);
                        if (result == MessageBoxResult.No)
                            break;
                    }
                    else
                    {
                        MessageBox.Show(ex.Message);
                        break;
                    }
                }
                if (token.IsCancellationRequested)
                    break;
                Dispatcher.BeginInvoke(new Action(() => Refresh()));
            }
            Dispatcher.BeginInvoke(new Action(() => Close()));
        }

        private void UpdateLable(string from, string to)
        {
            FromLb.Content = string.Format("Moving from: {0}", from);
            ToLb.Content = string.Format("To: {0}", to);
        }

        private void Refresh()
        {
            CopyProgress.Value += _percentsPerItem;
            Percents.Text = string.Format("{0}%", CopyProgress.Value);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!_task.IsCanceled || !_task.IsCompleted)
                _tokenSource.Cancel();
        }
    }
}
