﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using FileSystemClasses;
using System.Threading;
using System.IO;

namespace MyTotalCommander
{
    /// <summary>
    /// Interaction logic for CopyWindow.xaml
    /// </summary>
    public partial class CopyWindow : Window
    {
        private string _path;
        private int _total;
        private int _currentIndex;
        private double _percentsPerItem;
        private Task _task;
        private CancellationTokenSource _tokenSource;
        private CancellationToken _token;

        public CopyWindow(List<FSItemContent> copyContent, string path)
        {
            InitializeComponent();
            _path = path;
            _tokenSource = new CancellationTokenSource();
            _token = _tokenSource.Token;
            _total = copyContent.Count;
            _currentIndex = 0;
            _percentsPerItem = 100.0 / _total;
            CopyProgress.Value = 0;
            _task = Task.Factory.StartNew(() => Copy(copyContent, _token), _token);
        }

        private void Copy(List<FSItemContent> _copyContent, CancellationToken token)
        {
            FSItemContent _currentCopyingItem;
            while (_currentIndex < _total)
            {
                _currentCopyingItem = _copyContent[_currentIndex++];
                token.ThrowIfCancellationRequested();
                Dispatcher.BeginInvoke(new Action(() => UpdateLable(_currentCopyingItem.Name)));
                if (token.IsCancellationRequested)
                    break;
                try
                {
                    _currentCopyingItem.Copy(_path);
                }
                catch (UnauthorizedAccessException ex)
                {
                    MessageBox.Show(ex.Message);
                    break;
                }
                catch (IOException ex)
                {
                    if (_currentIndex == _total - 1)
                    {
                        MessageBoxResult result = MessageBox.Show(ex.Message + "\nContinue copying rest items?", "My Total Commander", MessageBoxButton.YesNo, MessageBoxImage.Question);
                        if (result == MessageBoxResult.No)
                            break;
                    }
                    else
                    {
                        MessageBox.Show(ex.Message);
                        break;
                    }
                }
                if (token.IsCancellationRequested)
                    break;
                Dispatcher.BeginInvoke(new Action(() => Refresh()));
            }
            Dispatcher.BeginInvoke(new Action(() => Close()));
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!_task.IsCanceled || !_task.IsCompleted)
                _tokenSource.Cancel();
        }

        private void CancelBtn_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("User abort!", "My Total Commander", MessageBoxButton.OKCancel, MessageBoxImage.Stop);
            if (result == MessageBoxResult.OK)
            {
                _tokenSource.Cancel();
                CurrentItem.Content = "Canceling...";
                CopyProgress.Value = 100;
                Percents.Text = string.Format("{0}%", CopyProgress.Value);
                Close();
            }
        }

        private void UpdateLable(string nomination)
        {
            CurrentItem.Content = string.Format("Copying: {0}", nomination);
        }

        private void Refresh()
        {
            CopyProgress.Value += _percentsPerItem;
            Percents.Text = string.Format("{0}%", CopyProgress.Value);
        }
    }
}
