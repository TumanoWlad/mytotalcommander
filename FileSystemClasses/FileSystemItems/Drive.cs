﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;

namespace FileSystemClasses
{
    public class Drive
    {

        private DriveInfo _info;

        public char Letter
        {
            get
            {
                return _info.Name.ToLower()[0];
            }
        }

        public DriveType Type
        {
            get
            {
                return _info.DriveType;
            }
        }
        
        private Drive(DriveInfo info)
        {
            _info = info;
        }

        public List<FSItemContent> GetContent()
        {
            return Drive.GetContent(_info.Name);
        }

        public static List<FSItemContent> GetContent(string path)
        {
            List<FSItemContent> content = new List<FSItemContent>();
            // rewrite with lambdas and parametrization
            Func<FileSystemInfo, bool> accessDenied = x => x.Attributes.HasFlag(FileAttributes.Hidden) || x.Attributes.HasFlag(FileAttributes.System);

            foreach (string s in Directory.GetDirectories(path))
            {
                DirectoryInfo dir = new DirectoryInfo(s);
                if (!accessDenied(dir))
                    content.Add(new FolderContent(dir));
            }

            foreach (string s in Directory.GetFiles(path))
            {
                FileInfo file = new FileInfo(s);
                if (!accessDenied(file))
                    content.Add(new FileContent(file));
            }

            return content;
        }

        public static List<Drive> ReadSystemDrives()
        {
            List<Drive> drives = new List<Drive>();
            Drive drive;
            foreach (var item in DriveInfo.GetDrives())
            {
                if (item.IsReady)
                    drive = new Drive(item);
                else
                    drive = new Drive(item);
                drives.Add(drive);
            }

            return drives;
        }

        public static bool Exists(string driveName)
        {
            if (driveName.Length > 3)
                return false;
            driveName = driveName.ToUpper();
            if (driveName[driveName.Length - 1] != '\\')
                driveName += "\\";
            IEnumerable<string> drives = from drive in DriveInfo.GetDrives()
                                  select drive.Name;
            if (drives.ToList().Contains(driveName))
                return true;
            return false;
        }
    }
}
