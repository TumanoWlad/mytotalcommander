﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileSystemClasses
{
    public abstract class FolderState
    {
        public void Rename(FolderContent folder, string newName) 
        {
            string newPath = Path.Combine(Directory.GetParent(folder.Path).FullName, newName);
            if (Directory.Exists(newPath))
                throw new System.IO.IOException();
            Directory.Move(folder.Path, newPath);
        }

        public void Copy(FolderContent folder, string destinationPath)
        {
            string destinationFolder = Path.Combine(destinationPath, folder.Name);
            CreateIfNotExists(destinationFolder);
            CopyContent(new DirectoryInfo(folder.Path), destinationFolder);
        }

        protected void CreateIfNotExists(string destination)
        {
            if (!Directory.Exists(destination))
                Directory.CreateDirectory(destination);
        }

        protected abstract void CopyContent(DirectoryInfo directory, string destination);

        public void Move(FolderContent folder, string destinationPath)
        {
            if (Char.ToLower(folder.Path[0]) == Char.ToLower(destinationPath[0]))
                Directory.Move(folder.Path, System.IO.Path.Combine(destinationPath, folder.Name));
            else
            {
                folder.Copy(destinationPath);
                Directory.Delete(folder.Path, true);
            }
        }

        public abstract void Delete(FolderContent folder);

        public abstract List<FSItemContent> GetContent(FolderContent folder);
    }
}
