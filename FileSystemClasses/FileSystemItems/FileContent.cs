﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using System.Diagnostics;
using Microsoft.VisualBasic.FileIO;

namespace FileSystemClasses
{
    public class FileContent : FSItemContent
    {
        public long Size
        {
            get;
            private set;
        }

        public new string Name
        {
            get
            {
                string retName;
                try
                {
                    retName = _name.Remove(_name.LastIndexOf("."));
                }
                catch
                {
                    retName = _name;
                }
                return retName;
            }
        }

        public string Extension
        {
            get
            {
                string extension;
                try
                {
                    extension = System.IO.Path.GetExtension(Path).Split('.')[1];
                }
                catch
                {
                    extension = "";
                }
                return extension;
            }
        }

        public FileContent(FileInfo info)
            : base(info)
        {
            Size = info.Length;
            _icon = Icon.ExtractAssociatedIcon(Path);
        }

        public override void Copy(string destinationPath)
        {
            if (!Directory.Exists(destinationPath))
                Directory.CreateDirectory(destinationPath);
            string newPath = System.IO.Path.Combine(destinationPath, _name);
            File.Copy(Path, newPath);
        }

        public override void Move(string destinationPath)
        {
            File.Move(Path, System.IO.Path.Combine(destinationPath, _name));
        }

        public override void Rename(string name)
        {
            File.Move(Path, System.IO.Path.GetDirectoryName(Path) + name);
        }

        public override void Delete()
        {
            FileSystem.DeleteFile(Path, UIOption.AllDialogs, RecycleOption.SendToRecycleBin);
        }

        public override void Open()
        {
            Process.Start(Path);
        }

        public override List<FSItemContent> GetContent()
        {
            return null;
        }

        public override void SetAsReturnItem()
        {
            throw new NotImplementedException();
        }
    }
}
