﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.VisualBasic.FileIO;

namespace FileSystemClasses
{
    public class FilledFolder : FolderState
    {
        protected override void CopyContent(DirectoryInfo directory, string destination)
        {
            FileInfo[] files = directory.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = System.IO.Path.Combine(destination, file.Name);
                file.CopyTo(temppath, false);
            }

            DirectoryInfo[] dirs = directory.GetDirectories();
            foreach (DirectoryInfo subdir in dirs)
            {
                FolderContent tempFolder = new FolderContent(subdir);
                tempFolder.Copy(destination);
            }
        }

        public override void Delete(FolderContent folder)
        {
            try
            {
                FileSystem.DeleteDirectory(folder.Path, UIOption.AllDialogs, RecycleOption.SendToRecycleBin);
            }
            catch (OperationCanceledException) { }
        }

        public override List<FSItemContent> GetContent(FolderContent folder)
        {
            return Drive.GetContent(folder.Path);
        }
    }
}
