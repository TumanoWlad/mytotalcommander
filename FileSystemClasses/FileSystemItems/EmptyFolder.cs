﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.VisualBasic.FileIO;

namespace FileSystemClasses
{
    public class EmptyFolder : FolderState
    {
        protected override void CopyContent(DirectoryInfo directory, string destination) { }

        public override void Delete(FolderContent folder)
        {
            FileSystem.DeleteDirectory(folder.Path, DeleteDirectoryOption.ThrowIfDirectoryNonEmpty);
        }

        public override List<FSItemContent> GetContent(FolderContent folder)
        {
            return new List<FSItemContent>();
        }
    }
}
