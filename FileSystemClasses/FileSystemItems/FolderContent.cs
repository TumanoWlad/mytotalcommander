﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;

namespace FileSystemClasses
{
    public class FolderContent : FSItemContent
    {

        private static FilledFolder filledState = new FilledFolder();
        private static EmptyFolder emptyState = new EmptyFolder();

        public static string Size
        {
            get
            {
                return "<DIR>";
            }
        }

        private FolderState state
        {
            get
            {
                if (Directory.GetFileSystemEntries(Path).Length == 0)
                    return emptyState;
                return filledState;
            }
        }


        public FolderContent(DirectoryInfo info) 
            : base(info) 
        {
            _icon = FileSystemClasses.Resources.Folder;
        }

        public static FolderContent GetFolderFromPath(string path)
        {
            if (!Directory.Exists(path))
                return null;
            FolderContent folder = null;
            DirectoryInfo dir;
            FileAttributes attr = File.GetAttributes(path);
            if (attr.HasFlag(FileAttributes.Directory))
            {
                dir = new DirectoryInfo(path);
            }
            else
            {
                FileInfo fileInfo = new FileInfo(path);
                dir = fileInfo.Directory;
            }
            if (!(dir.Attributes.HasFlag(FileAttributes.Hidden) || dir.Attributes.HasFlag(FileAttributes.System)))
                folder = new FolderContent(dir);
            return folder;
        }

        public override void Copy(string destinationPath)
        {
            state.Copy(this, destinationPath);
        }

        public override void Move(string destinationPath)
        {
            state.Move(this, destinationPath);
        }

        public override void Delete()
        {
            state.Delete(this);
        }

        public override void Rename(string newName)
        {
            state.Rename(this, newName);
        }

        public override void SetAsReturnItem()
        {
            _icon = Icon.FromHandle(FileSystemClasses.Resources.Return.GetHicon());
            _name = "_ _";
        }

        public override List<FSItemContent> GetContent()
        {
            return state.GetContent(this);
        }

        public override void Open()
        {
        }

    }
}
