﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using System.Windows;
using System.Windows.Media;
using System.Windows.Interop;

namespace FileSystemClasses
{
    public abstract class FSItemContent
    {
        protected FileSystemInfo _info;
        protected Icon _icon;
        protected string _name;

        public string Path
        {
            get
            {
                return _info.FullName;
            }
        }
        
        public string Name
        {
            get
            {
                return _name; ;
            }
        }

        public DateTime Date
        {
            get
            {
                return _info.LastWriteTime;
            }
        }

        public string StringAttributes
        {
            get
            {
                return BuildAttributesAbbreviation();
            }
        }

        public ImageSource IconSource
        {
            get
            {
                return Imaging.CreateBitmapSourceFromHIcon(
                _icon.Handle,
                Int32Rect.Empty,
                System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());
            }
        }

        public FSItemContent(FileSystemInfo info)
        {
            _info = info;
            _name = info.Name;
        }

        public bool HasAttribute(Enum attribute)
        {
            return _info.Attributes.HasFlag(attribute);
        }

        private string BuildAttributesAbbreviation()
        {
            StringBuilder abbreviation = new StringBuilder();

            if (_info.Attributes.HasFlag(FileAttributes.ReadOnly))
                abbreviation.Append("r");
            else
                abbreviation.Append("-");

            if (_info.Attributes.HasFlag(FileAttributes.Archive))
                abbreviation.Append("a");
            else
                abbreviation.Append("-");

            return abbreviation.ToString();
        }

        public abstract void Delete();

        public abstract void Move(string dest);

        public abstract void Copy(string dest);

        public abstract void Rename(string name);

        public abstract List<FSItemContent> GetContent();

        public abstract void SetAsReturnItem();

        public abstract void Open();
    }
}
